//
//  ContentView.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/23/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import SwiftUI

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height


struct ContentView: View {
    
    @ObservedObject var articlesViewModel: ArticlesViewModel
    
    var body: some View {
        TabView {
    
            NavigationView {
                List(articlesViewModel.topHeadlines) { article in
                        NavigationLink(destination: ArticleDetail(article: article)) {
                            Cell(article: article)
                            .onAppear {
                                self.articlesViewModel.loadMoreArticles("top-headlines", article)
                            }
                        }
                }.navigationBarTitle(Text("Top headlines"))
            }
            .tabItem {
                    Image(systemName: "t.square.fill")
                    Text("Top headlines")
            }
            
            NavigationView {
                List(articlesViewModel.everything) { article in
                        NavigationLink(destination: ArticleDetail(article: article)) {
                            Cell(article: article)
                            .onAppear {
                                self.articlesViewModel.loadMoreArticles("everything", article)
                            }
                        }
                }.navigationBarTitle(Text("Everything"))
            }
            .tabItem {
                    Image(systemName: "e.square.fill")
                    Text("Everything")
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(articlesViewModel: ArticlesViewModel(service: NewsService()))
    }
}
