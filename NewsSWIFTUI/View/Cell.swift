//
//  Cell.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/24/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import SwiftUI

struct Cell: View {
    
    var article: Article
    
    @State var image = Image("image-placeholder")
    
    func loadImage(_ urlString: String, completion: @escaping (_ data: Data) -> Void) {
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            DispatchQueue.main.async {
                completion(data)
            }
        }
        task.resume()
    }
    
    
    var body: some View {
        
        HStack(spacing: 0) {
            image.resizable()
                .frame(width: screenHeight/10, height: screenHeight/10, alignment: .center)
                .onAppear(perform: {
                    self.loadImage(self.article.urlToImage) { (data) in
                        self.image = Image(uiImage: UIImage(data: data) ?? UIImage())
                    }
                })
            VStack(spacing: 4) {
                Text(article.title)
                    .lineLimit(nil)
                    .multilineTextAlignment(.leading)
                    .font(.headline)
                    .padding()
                
                HStack {
                    Text(article.publishedAt)
                        .font(.footnote)
                    Spacer()
                    Text(article.author ?? "")
                        .font(.footnote)
                    
                }
                .padding(.bottom, 5)
                .padding(.trailing, 5)
            }
        }
    }
}

struct Cell_Previews: PreviewProvider {
    static var previews: some View {
        Cell(article: articlesResponse.first!)
    }
}
