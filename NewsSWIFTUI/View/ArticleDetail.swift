//
//  AcrticleDetail.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/26/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import SwiftUI

struct ArticleDetail: View {
    
    var article: Article
    
    @State var image = Image("image-placeholder")
    
    
    func loadImage(_ urlString: String, completion: @escaping (_ data: Data) -> Void) {
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            DispatchQueue.main.async {
                completion(data)
            }
        }
        task.resume()
    }
    
    
    var body: some View {
        ScrollView {
            VStack {
                image.resizable()
                    .frame(width: screenWidth, height: screenHeight/2, alignment: .center)
                    .onAppear(perform: {
                        self.loadImage(self.article.urlToImage) { (data) in
                            self.image = Image(uiImage: UIImage(data: data) ?? UIImage())
                        }
                    })
                Text(article.title)
                    .lineLimit(nil)
                    .font(.headline)
                    
                Text(article.content ?? "No content")
                    .padding()
            }
        }
    }
}

struct AcrticleDetail_Previews: PreviewProvider {
    static var previews: some View {
        ArticleDetail(article: articlesResponse[0])
    }
}
