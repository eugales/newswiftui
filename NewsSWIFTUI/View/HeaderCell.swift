//
//  HeaderCell.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/24/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import SwiftUI

struct HeaderCell: View {
    var body: some View {
        ZStack(alignment: .bottomLeading) {
            Image("woman")
                .resizable()
                .frame(width: screenWidth, height: screenHeight/3, alignment: .center)
            VStack {
                Text("Brad Pitt is the new toastmaster of awards season - CNN")
                    .lineLimit(nil)
                    .multilineTextAlignment(.leading)
                HStack {
                    Text("23 january 2020, 01:10")
                    Spacer()
                    Image("comment")
                        .resizable()
                        .frame(width: 20, height: 20, alignment: .center)
                        .clipShape(Circle())
                    Text("11")
                }
            }
            .padding()
            .border(Color.gray, width: 2)
        }
        .border(Color.gray, width: 2)
    }
}

struct HeaderCell_Previews: PreviewProvider {
    static var previews: some View {
        HeaderCell()
    }
}
