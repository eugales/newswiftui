//
//  ArticlesList.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/25/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import SwiftUI

struct ArticlesList: View {
    
    var categoryName: String
    var articles: [Article]
    
    var body: some View {
        NavigationView {
            List(articles) { article in
                NavigationLink(destination: ArticleDetail(article: article)) {
                    Cell(article: article)
                    
                }
            }
            .navigationBarTitle(Text(categoryName))
            
        }
    }
}

struct ArticlesList_Previews: PreviewProvider {
    static var previews: some View {
        ArticlesList(categoryName: "Articles", articles: ArticlesViewModel(service: NewsService()).topHeadlines)
    }
}
