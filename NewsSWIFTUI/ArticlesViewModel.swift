//
//  NewsResponseViewModel.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/25/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import Foundation


class ArticlesViewModel: ObservableObject {
    let service: NewsService
    
    @Published var topHeadlines = [Article]()
    @Published var everything = [Article]()
    
    init(service: NewsService) {
        self.service = service
    }
    
    func fetchTopHeadlines(page: Int = 1) {
        currentlyLoading = true
        service.getNews(for: "top-headlines", page: page) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                    case .success(let articles):
                        if !(self?.topHeadlines.elementsEqual(articles))! {
                            self?.topHeadlines.append(contentsOf: articles)
                        }
                    case .failure(let error):
                        self?.topHeadlines = []
                        fatalError(error.localizedDescription)
                }
                self?.currentlyLoading = false
            }
        }
    }
    
    func fetchEverything(page: Int = 1) {
        currentlyLoading = true
        service.getNews(for: "everything", page: page) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                    case .success(let articles):
                        self?.everything.append(contentsOf: articles)
                    case .failure(let error):
                        self?.everything = []
                        fatalError(error.localizedDescription)
                }
            }
            self?.currentlyLoading = false
        }
    }
    
    var currentlyLoading = false
    var topHeadlinesPageIndex = 1
    var everythingPageIndex = 1
    
    func shouldLoadMoreArticles(_ currentArticle: Article? = nil) -> Bool {
        if currentlyLoading {
            return false
        }
        
        guard let currentArticle = currentArticle  else { return true }
        guard let lastArticle = everything.last else { return true }
        return currentArticle.id == lastArticle.id
    }
    
    func loadMoreArticles(_ articlePath: String, _ currentArticle: Article? = nil) {
        if !shouldLoadMoreArticles(currentArticle) {
            return
        }
        
        switch articlePath {
            case "top-headlines":
                topHeadlinesPageIndex += 1
                fetchTopHeadlines(page: topHeadlinesPageIndex)
            case "everything":
                everythingPageIndex += 1
                fetchEverything(page: everythingPageIndex)
            default: return
        }
    }
}
