//
//  NewsResponse.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/23/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct NewsResponse: Hashable {
    var status: String
    var totalResults: Int
    var articles: [Article]
}

extension NewsResponse: Codable {
    enum CodingKeys: String, CodingKey {
        case status
        case totalResults
        case articles
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        status = try values.decode(String.self, forKey: .status)
        totalResults = try values.decode(Int.self, forKey: .totalResults)
        articles = try values.decode([Article].self, forKey: .articles)
    }
}
