//
//  Article.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/25/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import Foundation
import SwiftUI


struct Source: Hashable, Equatable {
    var id: String?
    var name: String
}

extension Source: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(String?.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
    }
}

struct Article: Identifiable, Hashable, Equatable {
    var id: UUID
    
    var source: Source
    var author: String?
    var title: String
    var description: String
    var url: String
    var urlToImage: String
    var publishedAt: String
    var content: String?
    
    init(id: UUID, source: Source, author: String?, title: String, description: String,
         url: String, urlToImage: String, publishedAt: String, content: String?) {
        
        self.id = id
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
    }
    
    // Compare all except :id for collection comparison
    static func == (lhs: Article, rhs: Article) -> Bool {
        return
            lhs.source == rhs.source &&
            lhs.author == rhs.author &&
            lhs.title == rhs.title &&
            lhs.description == rhs.description &&
            lhs.url == rhs.url &&
            lhs.urlToImage == rhs.urlToImage &&
            lhs.publishedAt == rhs.publishedAt &&
            lhs.content == rhs.content
    }
}

extension Article: Codable {
    enum CodingKeys: String, CodingKey {
        
        case id
        case source
        case author
        case title
        case description
        case url
        case urlToImage
        case publishedAt
        case content
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = UUID()
        source = try values.decode(Source.self, forKey: .source)
        author = try values.decode(String?.self, forKey: .author)
        title = try values.decode(String.self, forKey: .title)
        description = try values.decode(String.self, forKey: .description)
        url = try values.decode(String.self, forKey: .url)
        urlToImage = try values.decode(String.self, forKey: .urlToImage)
        publishedAt = try values.decode(String.self, forKey: .publishedAt)
        content = try values.decode(String?.self, forKey: .content)
    }
}
