//
//  Data.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/23/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI


let newsResponse: NewsResponse = load("response")
let articlesResponse: [Article] = newsResponse.articles

func load<T: Decodable>(_ filename: String, as type: T.Type = T.self) -> T {
    
    let data: Data
    
    guard let file =
        Bundle.main.url(forResource: filename, withExtension: "json")
        else { fatalError("Couldn't find \(filename) in main bundle.")}
    
    do {
        data = try Data(contentsOf: file)
    } catch  {
        fatalError("Couldn't load \(filename) from main bundle: \n\(error.localizedDescription)")
    }
    
    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self): \n\(error.localizedDescription)")
    }
}

