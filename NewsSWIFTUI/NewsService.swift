//
//  NewsService.swift
//  NewsSWIFTUI
//
//  Created by Adil Mamyrkhanov on 1/25/20.
//  Copyright © 2020 Adil Mamyrkhanov. All rights reserved.
//

import Foundation


class NewsService {
    
    typealias NewsResult = (Result<[Article], Error>) -> Void
    
    private let session: URLSession
    private let decoder: JSONDecoder
    
    init(session: URLSession = .shared, decoder: JSONDecoder = .init()) {
        self.session = session
        self.decoder = decoder
    }
    
    func getNews(for path: String, page: Int = 1, completion: @escaping NewsResult) {
        let urlString = "https://newsapi.org/v2/" + path + "?q=Apple&pageSize=15&apiKey=e65ee0938a2a43ebb15923b48faed18d&page=\(String(page) )"
        guard let url = URL(string: urlString) else { return }
        
        session.dataTask(with: url) { [weak self] data, _, error in
            if let error = error {
                completion(.failure(error))
            }
            
            do {
                guard let data = data else { return }
                let response = try self?.decoder.decode(NewsResponse.self, from: data)
                completion(.success(response?.articles ?? []))
            } catch {
                completion(.failure(error))
            }
        
        }.resume()
    }
}
